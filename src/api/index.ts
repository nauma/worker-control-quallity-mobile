import { BaseRequest } from "./BaseRequest"
import { AxiosRequest, AxiosConfigStructure } from "./AxiosRequest"

import AsyncStorage from '@react-native-community/async-storage'
//
export interface ApiResponseSuccess<Result> {
	status: boolean
	result: Result
}

export interface ApiResponseError {
	status: boolean
	error: {
		code?: number
		message: string
	}
}


export interface ApiResponseClientAuthorization {
	token: string
	user_id: string
}

export type ApiResponseType = ApiResponseSuccess<ApiResponseClientAuthorization> | ApiResponseError
//
export class API {
	public client: BaseRequest
	public config: AxiosConfigStructure = {
		baseURL: 'http://81.176.229.87:3535/api',
		timeout: 25 * 1000
	}

	public token: boolean = false
	public session: boolean = false

	constructor () {
		this.client = new AxiosRequest(this.config)
		this.loadToken()
	}

	async loadToken (): Promise<void> {
		const token = await AsyncStorage.getItem('token')

		if (!this.token && token !== null) {
			this.client.setHeader('token', token)
			this.token = true
		}
		//
		return
	}

	async setToken (token: string): Promise<void> {
		await AsyncStorage.setItem('token', token)
		await this.client.setHeader('token', token)
		//
		this.token = true
		//
		return
	}

	async clearToken (): Promise<void> {
		await AsyncStorage.clear()
		await this.client.delHeader('token')
		//
		this.token = false
		//
		return
	}

	async ClientAuthorization (email: string, password: string): Promise<void> {
		return this.client.post('/authorization/login', { email, password })
			.then((response: any) => {
				this.session = true
				//
				return Promise.resolve(response)
			})
	}

	async ClientProfile (): Promise<void> {
		await this.loadToken()
		//
		return this.client.get('/web/account', {})
			.then((response: any) => {
				this.session = true
				//
				return Promise.resolve(response)
			})
	}

	async ClientGetObjects (): Promise<any> {
		await this.loadToken()
		//
		return this.client.get('/web/objects', {})
	}

	async ClientAddObject (title: string, description: string = ''): Promise<any> {
		return this.client.post('/web/objects', { title, description })
	}

	async ClientGetObject (id: string): Promise<any> {
		await this.loadToken()
		//
		return this.client.get('/web/objects/' + id, {})
	}

	async ClientEditObject (_id: string, title: string, description: string = ''): Promise<any> {
		return this.client.post('/web/objects/' + _id, { _id, title, description })
	}

	async ClientDeleteObject (id: string) {
		return this.client.delete('/web/objects/' + id)
	}
}