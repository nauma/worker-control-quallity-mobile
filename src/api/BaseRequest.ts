export abstract class BaseRequest {
	client: any
	headers: HeadersType = {}

	abstract setHeader (key: string, value: string): BaseRequest
	abstract delHeader (key: string): BaseRequest
	abstract get (endpoint: string, params: any): Promise<ResponseType>
	abstract post (endpoint: string, body: any): Promise<ResponseType>
	abstract delete (endpoint: string): Promise<ResponseType>
	abstract request (endpoint: string, link: string, options: any): Promise<ResponseType>
}
//
export type HeadersType = Record<string, string>
export type ResponseType = Record<string, any> | Promise<void> | Promise<Record<string, any>>