import axios from 'axios'
//
import { BaseRequest, ResponseType, HeadersType } from './BaseRequest'
//
export class AxiosRequest implements BaseRequest {
	client: any = null
	headers: HeadersType = {}

	constructor (config: AxiosConfigStructure) {
		this.client = axios.create(config)
	}

	setHeader(key: string, value: string): AxiosRequest {
		this.headers[key] = value
		this.client.defaults.headers.common[key] = value
		//
		return this
	}

	delHeader(key: string): AxiosRequest {
		delete this.headers[key]
		delete this.client.defaults.headers.common[key]
		//
		return this
	}

	async get(endpoint: string, params?: Record<string, string>): Promise<ResponseType> {
		return this.client.get(`${endpoint}`, { params, headers: this.headers })
			.then((response: any) => Promise.resolve(response.data.hasOwnProperty('result') ? response.data.result : null))
			.catch((error: any) => Promise.reject(error.response.data ? error.response.data.error : null))
	}

	async post(endpoint: string, data: Record<string, any>): Promise<ResponseType> {
		return this.client.post(`${endpoint}`, data, { headers: this.headers })
			.then((response: any) => Promise.resolve(response.data.hasOwnProperty('result') ? response.data.result : null))
			.catch((error: any) => Promise.reject(error.response.data ? error.response.data.error : null))
	}

	async delete(endpoint: string): Promise<ResponseType> {
		return this.client.delete(`${endpoint}`, {}, { headers: this.headers })
			.then((response: any) => Promise.resolve(response.data.hasOwnProperty('result') ? response.data.result : null))
			.catch((error: any) => Promise.reject(error.response.data ? error.response.data.error : null))
	}

	async request(method: string, endpoint: string, options: any): Promise<ResponseType> {
		return this.client({ method, url: endpoint, ...options })
			.then((response: any) => Promise.resolve(response.data.hasOwnProperty('result') ? response.data.result : null))
			.catch((error: any) => Promise.reject(error.response.data ? error.response.data.error : null))
	}
}

export interface AxiosConfigStructure {
	baseURL: string,
	timeout?: number,
	headers?: HeadersType
	responseType?: string | any
}