import 'react-native-gesture-handler';
import React from 'react';
import { StatusBar, Text } from 'react-native';

import { createStore } from "redux";
import { Provider } from 'react-redux'
//
import Reducers from './store'
import Navigation from './navigation'
//
const store = createStore(Reducers)
//
const App = () => {
  return (
    <Provider store={store}>
      <StatusBar barStyle={'light-content'} />
      <Navigation />
    </Provider>
  );
};

export default App;