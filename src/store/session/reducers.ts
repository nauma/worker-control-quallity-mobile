import {
	SET_TOKEN,
	CLEAR_TOKEN,

	SessionState,
	SessionActionTypes,
} from "./types";
//
const initialState: SessionState = {
	token: null
}
//
export const sessionReducer = function (state = initialState, action: SessionActionTypes) {
	switch (action.type) {
		case SET_TOKEN:
			return { ...state, token: action.payload }

		case CLEAR_TOKEN:
			return { ...state, token: null }

		default:
			console.log(`Session reducer, nothing to mutate...`)
			return state
	}
}