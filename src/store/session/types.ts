// types
export const SET_TOKEN: string = 'session/set_token'
export const CLEAR_TOKEN: string = 'session/clear_token'
//
export interface SessionState {
	token: string | null
}
//
interface SetTokenAction {
	type: typeof SET_TOKEN
	payload: string
}

interface ClearTokenAction {
	type: typeof CLEAR_TOKEN
	payload: boolean | null
}

export type SessionActionTypes = SetTokenAction | ClearTokenAction