import {
	SET_TOKEN,
	CLEAR_TOKEN,

	SessionActionTypes
} from "./types";

export function SetSessionToken (token: string): SessionActionTypes {
	return {
		type: SET_TOKEN,
		payload: token
	}
}

export function ClearSessionToken (): SessionActionTypes {
	return {
		type: CLEAR_TOKEN,
		payload: null
	}
}