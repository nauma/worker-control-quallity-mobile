// types
export const SET_LOADING: string = 'object/set_loading'
export const SET_OBJECTS: string = 'object/set_objects'
export const SET_OBJECT: string = 'object/set_object'
//
export interface ObjectState {
	objects: Array<ObjectStructure>
	object: ObjectStructure | null
	loading: Boolean
}

export interface ObjectStructure {
	_id: string
	title: string
	description: string
}
//
interface SetLoadingAction {
	type: typeof SET_LOADING
	payload: boolean
}

interface SetObjectsAction {
	type: typeof SET_OBJECTS
	payload: Array<ObjectStructure>
}

interface SetObjectAction {
	type: typeof SET_OBJECT,
	payload: ObjectStructure
}

export type ObjectActionTypes = SetLoadingAction | SetObjectsAction | SetObjectAction
