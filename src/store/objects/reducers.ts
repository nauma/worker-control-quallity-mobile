import {
	SET_LOADING,
	SET_OBJECTS,
	SET_OBJECT,
	//
	ObjectState,
	ObjectActionTypes,
} from "./types";
//
const initialState: ObjectState = {
	objects: [],
	object: null,
	loading: false
}
//
export const objectReducer = function (state = initialState, action: ObjectActionTypes) {
	switch (action.type) {
		case SET_LOADING:
			return { ...state, loading: action.payload }
		case SET_OBJECTS:
			return { ...state, objects: action.payload }
		case SET_OBJECT:
			return { ...state, object: action.payload }
		default:
			console.log(`Object reducer, nothing to mutate...`)
			return state
	}
}