import {
	SET_LOADING,
	SET_OBJECTS,
	SET_OBJECT,

	ObjectStructure,
	ObjectActionTypes
} from "./types";

export function SetObjectLoading (loading: boolean): ObjectActionTypes {
	return {
		type: SET_LOADING,
		payload: loading
	}
}

export function SetObjectsList (objects: Array<ObjectStructure>): ObjectActionTypes {
	return {
		type: SET_OBJECTS,
		payload: objects
	}
}

export function SetObjectItem (object: ObjectStructure): ObjectActionTypes {
	return {
		type: SET_OBJECT,
		payload: object
	}
}