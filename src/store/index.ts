import { combineReducers } from "redux";
//
import { objectReducer } from './objects/reducers'
//
const rootReducer = combineReducers({
	object: objectReducer,
})
//
export default rootReducer