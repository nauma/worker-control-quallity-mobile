import React from 'react'
import {
	Text,
	Card,
	CardItem,
	Item,
	Input,
	Icon, Button, Body
} from "native-base";
//
import Styles from './Main.style'

/**
 * Client login form
 * @return {Component}
 */
export interface FormProps {
	email: string
	password: string
	disabled?: boolean

	errorMessage: string
	formValidation: any

	onChangeEmail?: any
	onChangePassword?: any
	onSubmit?: any
}

export function Form (props: FormProps) {
	return <Card transparent style={Styles.content}>
		<CardItem>
			<Item
				rounded
				error={props.formValidation.email === true || undefined}
				success={props.formValidation.email === false}
			>
				<Icon type='FontAwesome' name='envelope' style={Styles.icon} active={true} />
				<Input placeholder='Почта' defaultValue={props.email} onChangeText={props.onChangeEmail} />
			</Item>
		</CardItem>
		<CardItem>
			<Item
				rounded
				error={props.formValidation.password === true || undefined}
				success={props.formValidation.password === false}
			>
				<Icon type='FontAwesome' name='lock' style={Styles.icon} active={false} />
				<Input placeholder='Пароль' defaultValue={props.password} onChangeText={props.onChangePassword} />
			</Item>
		</CardItem>
		{ props.errorMessage.length > 0 && <CardItem>
			<Body style={Styles.errorMessage}>
				<Text style={Styles.errorMessageText}>{ props.errorMessage }</Text>
			</Body>
		</CardItem>}
		<CardItem>
			<Body>
				<Button block rounded success onPress={props.onSubmit} disabled={props.email.length === 0 || props.password.length === 0 || props.disabled}>
					<Text>Войти</Text>
				</Button>
			</Body>
		</CardItem>
	</Card>
}

Form.defaultProps = {
	email: '',
	password: '',
	disabled: false,

	errorMessage: '',
	formValidation: undefined,

	onChangeEmail: () => {},
	onChangePassword: () => {},
	onSubmit: () => {},
};


export function Header (): React.ReactElement {
	return <Card transparent style={Styles.block}>
		<CardItem>
			<Text style={Styles.textTitle}>Worker Control Quallity</Text>
		</CardItem>
	</Card>
}

export function Footer (): React.ReactElement {
	return <Card transparent style={Styles.block}>
		<CardItem>
			{/*<Button bordered rounded dark>*/}
			{/*	<Text>Я охранник</Text>*/}
			{/*</Button>*/}
		</CardItem>
	</Card>
}