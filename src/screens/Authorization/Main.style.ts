import { StyleSheet } from "react-native";
//
const Styles = StyleSheet.create({
	icon: { fontSize: 20, marginLeft: 10, color: 'gray' },
	block: { flex: 1, justifyContent: 'flex-end', alignItems: 'center' },
	content: { flex: 2 },
	textTitle: { fontSize: 25 },
	errorMessage: { alignItems: 'center' },
	errorMessageText: { color: 'red' }
})
//
export default Styles