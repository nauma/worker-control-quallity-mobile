import React, { Component } from 'react'
import { Alert } from "react-native"
import { Container } from 'native-base'
import validator from 'validator'
//
import { API } from '../../api'
import * as View from './Main.view'
//
export type AuthorizationScreenProps = {
	navigation: any
}

export type AuthorizationScreenState = {
	email: string
	password: string
	loading: boolean

	errorEmail: boolean
	errorPassword: boolean
	errorMessage: string
}

export default class AuthorizationScreen extends Component<AuthorizationScreenProps, AuthorizationScreenState> {
	state: AuthorizationScreenState = {
		email: '',
		password: '',
		loading: false,
		//
		errorEmail: true,
		errorPassword: true,
		errorMessage: ''
	}

	private Api: API = new API()

	get formValidation (): any {
		const { email, password, errorEmail, errorPassword } = this.state
		// 
		return {
			email: email.length > 0 ? errorEmail : undefined,
			password: password.length > 0 ? errorPassword : undefined
		}
	}

	async formChange (type: string, text: string): Promise<void> {
		if (type === 'email') {
			if (!validator.isLength(text, { min: 8, max: 50 })) {
				await this.setState({ email: text, errorEmail: true, errorMessage: 'Почта не валидная, длина должна быть больше 8 символов, но не более 50' })
			}
			else if (!validator.isEmail(text)) {
				await this.setState({ email: text, errorEmail: true, errorMessage: 'Почта не валидная :(' })
			}
			else {
				await this.setState({ email: text, errorEmail: false, errorMessage: '' })
			}
			return
		} else if (type === 'password') {
			if (!validator.isLength(text, { min: 6, max: 30 })) {
				await this.setState({ password: text, errorPassword: true, errorMessage: 'Пароль не валидный, почта должна быть не меньше 6 символов, но не более 30' })
			}
			else if (!validator.isAlphanumeric(text)) {
				await this.setState({ password: text, errorPassword: true, errorMessage: 'Пароль не валидный :(' })
			}
			else {
				await this.setState({ password: text, errorPassword: false, errorMessage: '' })
			}
		}
	}

	async formSubmit (): Promise<void> {
		const { email, password } = this.state
		//
		try {
			const response: any = await this.Api.ClientAuthorization(email, password)
			await this.Api.setToken(response.token)
			//
			this.props.navigation.replace({ routeName: 'ObjectMain' })
		} catch (error) {
			this.setState({ errorMessage: error.message })
		}
	}

	render(): React.ReactNode {
		const { email, password, loading, errorMessage } = this.state
		//
		return <Container>
			<View.Header />
			<View.Form
				email={email}
				password={password}

				errorMessage={errorMessage}
				formValidation={this.formValidation}

				onChangeEmail={this.formChange.bind(this, 'email')}
				onChangePassword={this.formChange.bind(this, 'password')}
				onSubmit={this.formSubmit.bind(this)}
				disabled={this.formValidation === false || loading}
			/>
			<View.Footer />
		</Container>
	}
}