import React, { Component } from 'react'
import { Alert } from 'react-native'
import { Button, Card, CardItem, Body, Text } from 'native-base'
import { connect } from 'react-redux'
//
import * as View from './Main.view'
import { API } from '../../api'
import * as mapDispatch from '../../store/objects/actions'
//
export type ObjectScreenProps = {
	navigation: any
	objects: Array<any>

	SetObjectsList: Function
}

class ObjectScreen extends Component<ObjectScreenProps> {
	static navigationOptions = {
		title: 'Удаление объекта'
  }

  private Api: API = new API()
  
  async onSubmit (): Promise<void> {
    const id: string = this.props.navigation.state.params && this.props.navigation.state.params._id
    // 
    try {
      await this.Api.ClientDeleteObject(id)
    } catch (e) {
      Alert.alert('Ошибка!', 'Ошибка удаления объекта, повторите попытку позже')
    }

    try {
      const result = await this.Api.ClientGetObjects()
      // 
      this.props.SetObjectsList(result)
    } catch (e) {
      Alert.alert('Ошибка!', 'Ошибка получения объектов, повторите попытку позже')
    }

    this.props.navigation.replace('ObjectMain')
  }

	render (): React.ReactNode {
    return <View.AppContent>
			<Card transparent>
      <CardItem>
          <Body>
            <Text>Вы действительно уверены что хотите удалить данный объект?</Text>
          </Body>
        </CardItem>
        <CardItem>
          <Body>
            <Button rounded bordered danger onPress={this.onSubmit.bind(this)}>
              <Text>Удалить</Text>
            </Button>
          </Body>
        </CardItem>
      </Card>
		</View.AppContent>
	}
}
// 
export default connect(null, mapDispatch)(ObjectScreen)