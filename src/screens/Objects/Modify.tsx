import React, { Component } from 'react'
import { Alert } from 'react-native'
import validator from 'validator'
import { connect } from 'react-redux'
//
import { API } from '../../api'
import * as mapDispatch from '../../store/objects/actions'
import * as View from './Main.view'
//
export type ObjectScreenProps = {
	navigation: any

	SetObjectsList: Function
}
export type ObjectScreenState = {
	_id: string | null
	title: string
	description: string

	errorTitle: boolean
	errorDescription: boolean
	errorMessage: string
}
//
class ObjectScreen extends Component<ObjectScreenProps, ObjectScreenState> {
	static navigationOptions = ({ navigation }: any) => {
		return {
			title: navigation.state.params && navigation.state.params.hasOwnProperty('_id') ? 'Редактирование объекта' : 'Добавление объекта'
		}
	}

	state: ObjectScreenState = {
		_id: null,
		title: '',
		description: '',

		errorTitle: false,
		errorDescription: false,
		errorMessage: '',
	}

	private Api: API = new API()

	async componentDidMount (): Promise<void> {
		const id: string | null = this.props.navigation.state.params && this.props.navigation.state.params._id || null
		// 
		try {
			if (id === null) return
			// 
			const result = await this.Api.ClientGetObject(id)
			// 
			this.setState({ _id: id, title: result.title, description: result.description })
		} catch (e) {

		}
	}

	get formValidation (): any {
		const { title, description, errorTitle, errorDescription } = this.state
		// 
		return {
			title: title.length > 0 ? errorTitle : undefined,
			description: description.length > 0 ? errorDescription : undefined
		}
	}

	formChange (key: string, value: string): void {
		if (key === 'title') {
			if (!validator.isAlphanumeric(value)) {
				this.setState({ title: value, errorTitle: true, errorMessage: 'Название может состоять только из цифр и латыницы' })
			}
			else if (!validator.isLength(value, { min: 4, max: 20 })) {
				this.setState({ title: value, errorTitle: true, errorMessage: 'Название должно быть не менее 4 символов и не более 20' })
			}
			else {
				this.setState({ title: value, errorTitle: false, errorMessage: '' })
			}
			return
		}

		if (key === 'description') {
			if (!validator.isAlphanumeric(value)) {
				this.setState({ description: value, errorDescription: true, errorMessage: 'Описание может состоять только из цифр и латыницы' })
			}
			else if (!validator.isLength(value, { max: 100 })) {
				this.setState({ description: value, errorDescription: true, errorMessage: 'Описание должно быть не более 100 символов' })
			}
			else {
				this.setState({ description: value, errorDescription: false, errorMessage: '' })
			}
		}
		return 
	}

	async formSubmit (): Promise<void> {
		const { _id, title, description } = this.state
		// 
		try {
			if (_id !== null) {
				await this.Api.ClientEditObject(String(_id), title, description)
			} else {
				await this.Api.ClientAddObject(title, description)
			}

			Alert.alert('Успех!', _id !== null ? 'Данные успешно обновлены' : 'Объект успешно добавлен')
		} catch (e) {
			Alert.alert('Ошибка!', 'Убедитесь в правильности данных')
		}

		try {
      const result = await this.Api.ClientGetObjects()
      // 
      this.props.SetObjectsList(result)
    } catch (e) {
      Alert.alert('Ошибка!', 'Ошибка получения объектов, повторите попытку позже')
    }

    this.props.navigation.replace('ObjectMain')
	}

	render (): React.ReactNode {
		const { _id, title, description, errorMessage } = this.state
		// 
		return <View.AppContent>
			<View.ObjectContent
				view='edit'
				_id={_id}
				title={title}
				description={description}

				errorMessage={errorMessage}
				formValidation={this.formValidation}
				onChangeTitle={this.formChange.bind(this, 'title')}
				onChangeDescription={this.formChange.bind(this, 'description')}
				onSubmit={this.formSubmit.bind(this)}
			
			></View.ObjectContent>
		</View.AppContent>
	}
}
// 
export default connect(null, mapDispatch)(ObjectScreen)