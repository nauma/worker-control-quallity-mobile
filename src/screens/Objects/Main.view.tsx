import React from 'react'
import {Card, Left, Right, Body, Container, Content, Text, CardItem, Input, Item, Button} from "native-base"
// 
import Styles from './Main.style'
//
export function AppContent (props: any) {
	return <Container>
		<Content padder>
			{props.children}
		</Content>
	</Container>
}
//
export interface ObjectBlockProps {
	_id: string
	title: string
	description: string

	onClick?: void
}

export function ObjectBlock (props: ObjectBlockProps): React.ReactElement {
	return <Card>
		<CardItem button onPress={props.onClick || (() => {})}>
			<Left>
				<Body>
					<Text>{ props.title }</Text>
				</Body>
			</Left>
			<Right>
				<Body>
					<Text>{ props._id }</Text>
				</Body>
			</Right>
		</CardItem>
		<CardItem>
			<Body>
				<Text>{ props.description }</Text>
			</Body>
		</CardItem>
	</Card>
}

ObjectBlock.defaultProps = {
	onClick: () => {}
}

//
export interface ObjectContentProps {
	view?: string
	_id?: string | null
	title: string
	description: string

	formValidation?: any
	errorMessage?: string

	onChangeTitle?: Function
	onChangeDescription?: Function
	onSubmit?: Function
}

export function ObjectContent (props: ObjectContentProps): React.ReactElement {
	console.log(1, props.view === 'edit' && (props.errorMessage && props.errorMessage.length > 0))
	return (<Card transparent>
		<CardItem>
			<Item>
				<Text>Название: { props.view === 'get' ? <Text>{ String(props.title) }</Text> : null }</Text>
				{ props.view === 'edit' && <Input rounded placeholder='Введите название' defaultValue={props.title} onChangeText={props.onChangeTitle || (() => {})} /> }
			</Item>
		</CardItem>

		<CardItem>
			<Item>
				<Text>Описание: { props.view === 'get' ? <Text>{ String(props.description) }</Text> : null }</Text>
				{ props.view === 'edit' && <Input rounded placeholder='Введите описание' defaultValue={props.description} onChangeText={props.onChangeDescription || (() => {})} /> }
			</Item>
		</CardItem>

		{ Boolean(props.view === 'edit' && (props.errorMessage && props.errorMessage.length > 0)) && <CardItem>
			<Body style={Styles.errorMessage}>
				<Text style={Styles.errorMessageText}>{ String(props.errorMessage) }</Text>
			</Body>
		</CardItem>}
		
		{ Boolean(props.view === 'edit') ? <CardItem>
			<Body>
				<Button onPress={props.onSubmit || (() => {})} rounded bordered success>
					{ props._id !== null ? <Text>Изменить</Text> : <Text>Добавить</Text> }
				</Button>
			</Body>
		</CardItem> : null }
	</Card>)
}

ObjectContent.defaultProps = {
	view: 'get',
	_id: null,
	title: '',
	description: '',

	errorMessage: '',

	onChangeTitle: () => {},
	onChangeDescription: () => {},
	onSubmit: () => {}
}