import { StyleSheet } from "react-native";
//
const Styles = StyleSheet.create({
	content: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	errorMessage: { alignItems: 'center' },
	errorMessageText: { color: 'red' }
})
//
export default Styles