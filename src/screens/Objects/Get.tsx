import React, { Component } from 'react'
import { Body, Button, Text } from 'native-base'
import { connect } from 'react-redux'
//
import { API } from '../../api'
import * as View from './Main.view'
import * as mapDispatch from '../../store/objects/actions'
import { Alert } from 'react-native'
//
export type ObjectScreenProps = {
	navigation: any
	object: any

	// SetObjectsList: Function
	SetObjectItem: Function
}
export type ObjectScreenState = {}
// 
const mapState = (state: any) => ({
	object: state.object.object,
	loading: state.object.loading
})
//
class ObjectScreen extends Component<ObjectScreenProps, ObjectScreenState> {
	static navigationOptions = ({ navigation }: any) => ({
		title: 'Объект',
		headerRight: () => <Body style={{ flexDirection: 'row' }}>
			<Button transparent onPress={() => navigation.push('ObjectModify', { _id: navigation.state.params._id })}>
				<Text>Ред.</Text>
			</Button>
			<Button transparent onPress={() => navigation.push('ObjectRemove', { _id: navigation.state.params._id })}>
			<Text>Удалить</Text>
		</Button>
		</Body>
	})

	private Api: API = new API()

	async componentDidMount (): Promise<void> {
		const id: string | null = this.props.navigation.state.params && this.props.navigation.state.params._id || null

		try {
			const result = await this.Api.ClientGetObject(String(id))
			// 
			this.props.SetObjectItem(result)
		} catch (e) {
			Alert.alert('Ошибка!', 'Ошибка загрузки данных')
		}
	}

	render (): React.ReactNode {
		const object = this.props.object
		// 
		return <View.AppContent>
			<View.ObjectContent view='get' { ...object } />
		</View.AppContent>
	}
}
// 
export default connect(mapState, mapDispatch)(ObjectScreen)