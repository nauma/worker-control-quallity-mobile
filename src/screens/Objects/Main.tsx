import React, { Component } from 'react'
import { Alert } from 'react-native'
import { Button, Icon, Text } from 'native-base'
import { connect } from 'react-redux'
//
import { API } from '../../api'
import * as View from './Main.view'
import * as mapDispatch from '../../store/objects/actions'
// 
export type ObjectScreenProps = {
	navigation: any
	objects: Array<any>

	SetObjectsList: Function
}
export type ObjectScreenState = {}
// 
const mapState = (state: any) => ({
	objects: state.object.objects,
	loading: state.object.loading
})
//
class ObjectScreen extends Component<ObjectScreenProps, ObjectScreenState> {
	static navigationOptions = ({ navigation }: any) => ({
		title: 'Объекты',
		headerRight: () => <Button transparent onPress={() => navigation.push('ObjectModify')}>
			<Icon type={'FontAwesome'} name={'plus'} style={{fontSize: 20, paddingRight: 5, color: 'green'}} />
		</Button>
	})

	static defaultProps = {
		objects: []
	}

	private Api: API = new API()

	async componentDidMount (): Promise<void> {
		try {
			const result = await this.Api.ClientGetObjects()
			// 
			this.props.SetObjectsList(result)
		} catch (e) {
			Alert.alert('Ошибка!', 'Ошибка получения объектов, повторите попытку позже')
		}

	}

	render (): React.ReactNode {
		const objects = this.props.objects
		// 
		return <View.AppContent>
			{ objects.length === 0 && <Text>Пусто...</Text> }
			{ objects.map((object: any) =>
				<View.ObjectBlock key={object._id} { ...object } onClick={() => this.props.navigation.push('ObjectGet', { _id: object._id })} />)
			}
		</View.AppContent>
	}
}
// 
export default connect(mapState, mapDispatch)(ObjectScreen)