import React, { Component } from 'react'
//
import { Container, Spinner, Text } from "native-base"
import { API } from "../../api"
import Styles from './Main.style'
//
export type ObjectScreenProps = {
	navigation: any
}

export default class SplashScreen extends Component<ObjectScreenProps> {
	private Api: API = new API()

	async componentDidMount(): Promise<void> {
		try {
			await this.Api.ClientProfile()

			await this.props.navigation.replace({ routeName: 'ObjectMain' });
		} catch (e) {
			this.props.navigation.replace('Authorization')
		}
	}

	render () {
		return <Container style={Styles.container}>
			<Spinner />
		</Container>
	}
}