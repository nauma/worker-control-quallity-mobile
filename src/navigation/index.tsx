import React from 'react'
//
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
//
import SplashScreen from '../screens/Splash/Main';
import AuthorizationScreen from '../screens/Authorization/Main';
import ObjectMainScreen from '../screens/Objects/Main';
import ObjectGetScreen from '../screens/Objects/Get';
import ObjectModifyScreen from '../screens/Objects/Modify';
import ObjectRemoveScreen from '../screens/Objects/Remove';
//
const AppNavigator = createStackNavigator({
	Splash: {
		screen: SplashScreen,
		navigationOptions: {
			header: () => null
		}
	},

	Authorization: {
		screen: AuthorizationScreen,
		navigationOptions: {
			header: () => null
		}
	},

	ObjectMain: {
		screen: ObjectMainScreen,
		navigationOptions: ObjectMainScreen.navigationOptions
	},

	ObjectGet: {
		screen: ObjectGetScreen,
		navigationOptions: ObjectGetScreen.navigationOptions
	},

	ObjectModify: {
		screen: ObjectModifyScreen,
		navigationOptions: ObjectModifyScreen.navigationOptions
	},

	ObjectRemove: {
		screen: ObjectRemoveScreen,
		navigationOptions: ObjectRemoveScreen.navigationOptions
	},
}, { initialRouteName: 'Splash' });

export default createAppContainer(AppNavigator);